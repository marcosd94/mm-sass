app.service('BaseService', ['$http', 'SessionService', '$rootScope', '$mdDialog', function ($http, $rootScope, $mdDialog) {
    return {
        recurso: '',
        /**
         * Realiza un get para obtener la lista de paginada de lotes
         * @function
         */
        listar: function (params) {
            return $http.get(App.REST_BASE + this.recurso+"/listar", {
                params: params
            });
        },

        /**
         * Realiza un post para guardar el nuevo lote
         * @function
         */
        crear: function (params) {

            return $http.post(App.REST_BASE + this.recurso+'/insertar', params);
        },

        /**
         * Realiza un post para actualizar la plantilla
         * @function
         */
        actualizar: function (params) {
            return $http.put(App.REST_BASE + this.recurso+'/modificar', params);
        },

        /**
         * Realiza un get para obtener una plantilla específica por su id.
         * @function
         */
        obtener: function (params) {
            var filtros = encodeURIComponent(
                angular.toJson(params));

            return $http.get(App.REST_BASE + this.recurso + "/listar?filtros="+filtros, {
            });
        },

        /**
         * Realiza un delete para borrar un registro específico por su id.
         * @function
         */
        eliminar: function (params) {
            return $http.delete(App.REST_BASE + this.recurso + "/"+params.id);
        },

        /**
         * Realiza un delete para borrar un registro específico por su id.
         * @function
         */
        eliminarCascada: function (url, params) {
            return $http.delete(App.REST_BASE+ this.recurso + "/"+ url + "/"+params.id);
        },

        getDatosUsuario: function (codigoUsuario,idUsuario, idPersona) {
            var params =
                {
                    "codigoUsuario": codigoUsuario,
                    "idUsuario": idUsuario,
                    "idPersona": idPersona
                };
                this.deleteUndefinedValues(params);
            var filtros = encodeURIComponent(
                angular.toJson(params));

            return $http.get(App.REST_BASE + 'usuario-view/vista?filtros='+filtros);
        },
        deleteUndefinedValues : function (object) {
            Object.keys(object).forEach(function (key) {
                if (!object[key]) {
                    delete object[key];
                }
            });
        },

        numberFormater : function (n, c, d, t) {
            var c = isNaN(c = Math.abs(c)) ? 2 : c,
                d = d == undefined ? "," : d,
                t = t == undefined ? "." : t,
                s = n < 0 ? "-" : "",
                i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        },

        isUndefined: function (property) {
            return typeof property == "undefined";
        }
}
}]);

/**
 * @class
 * Controller que implementa el formulario de alta y modificación de lotes.
 *
 * @name gfd.controller#LoteFormCtrl
 * @author <a href = "mailto:maximiliano.baez@konecta.com.py"> Maximiliano Báez </a>
 */
app.controller('BaseFormCtrl', ['$scope', '$routeParams', '$timeout', '$location', 'ConfirmService',
    function ($scope, $routeParams, $timeout, $location, confirmService) {

        $scope.path = "/" +$location.$$path.split("/")[1] + "/";

        /**
         * Variable que se utiliza para desabilitar botones
         */
        $scope.disabledButtonSave = false;

        /**
         * Url base del formulario
         * @field
         * @type {Object}
         */
        //$scope.uri = "";

        /**
         * Service utilizdo para recuperar los datos y realizar las operaciones.
         * @field
         * @type {Object}
         */
        //$scope.service = null;

        /**
         * Objeto que corresponde al recurso sobre se el cual se realizan las operaciones.
         * @field
         * @type {Object}
         */
        $scope.recurso = {};

        /**
         * Determina el modo de la pantalla
         * @returns {Boolean} True si está en modo creación, False en caso contrario.
         */
        $scope.isCrear = function () {
            return typeof $routeParams.id == "undefined";
        };

        /**
         * Se encarga de persistir los datos del modelo.
         */
        $scope.guardar = function () {
            if (this.isCrear()) {
                this.crearRecurso();
            } else {
                this.editarRecurso();
            }
        };

        /**
         * Se encarga de registrar un nuevo recurso.
         * de edición.
         */
        $scope.crearRecurso = function () {
            var confirm = confirmService.showConfirm('¿Está seguro de guardar el recurso?')
                .then(function(answer) {
                    $scope.disabledButtonSave = true;
                    $scope.service.crear($scope.recurso)
                        .then($scope.guardarSuccess
                            , function (data, code) {
                                $scope.disabledButtonSave = false;
                                Message.error("No se pudo realizar la operación");
                            });
                }, function() {
                    return;
                });
        };

        /**
         * Se encarga de actualizar los datos del recurso.
         */
        $scope.editarRecurso = function () {
            var confirm = confirmService.showConfirm('¿Está seguro de modificar el recurso?')
                .then(function(answer) {
                    $scope.disabledButtonSave = true;
                    $scope.disabledButtonSave = true;
                    return $scope.service.actualizar($scope.recurso)
                        .then($scope.guardarSuccess
                            , function (data, code) {
                                $scope.disabledButtonSave = false;
                                Message.error("No se pudo realizar la operación");
                            });
                }, function() {
                    return;
                });
        };

        /**
         * Se encarga de manejar el success de las peticiones
         * @param {object} data la respuesta de la petición
         */
        $scope.guardarSuccess = function (response) {
            $scope.disabledButtonSave = false;
            Message.ok("El recurso se ha registrado exitosamente.");
            console.log($scope.uri + response.data.id + "/ver");
            $location.url($scope.uri + response.data.id + "/ver");
        };

        /**
         * Se encarga de obtener los datos del recurso siempre y cuando la pantalla esté en modo
         * de edición.
         */
        $scope.getRecurso = function () {
            this.service.obtener($routeParams)
                .then(function (response) {
                    $scope.recurso = response.data.lista[0];
                },function (data, code) {
                    Message.error("No se pudo realizar la operación");
                });
        };

        /**
         * Constructor / Entrypoint
         * @constructor
         */
        (function initialize() {
            if (!$scope.isCrear()) {
                $scope.getRecurso();
            }
        })();
    }
]);

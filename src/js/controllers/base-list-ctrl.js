/**
 * @class
 * Controller que implementa la busqueda y listado de lotes.
 *
 * @name gfd.controller#LoteListCtrl
 * @author <a href = "mailto:maximiliano.baez@konecta.com.py"> Maximiliano Báez </a>
 */
app.controller('BaseListCtrl', ['$scope','$location', 'ConfirmService',
    function ($scope, $location, confirmService) {

        /**
         *
         */
        $scope.path = $location.$$path;

        $scope.loading = true;

        /**
         * Inicializacion de objeto
         */
        $scope.filterBy = {};

        /**
         * @field
         * Parametros de configuración de la grilla
         */
        $scope.init = {
            'count': 20,
            'page': 1,
            'sortBy': "id",
            'sortOrder': 'DESC',
            'filterBase': 1
        };

        /**
         * Se encarga de limpiar los criterios del filtrado.
         * @function
         */
        $scope.limpiar = function () {
            $scope.filterBy = {};
        };

        /**
         * Array que contiene los datos de configuración de la grilla
         * @type Array
         * @field
         */
        $scope.config = {
            "rows": [],
            "pagination": {
                "count": $scope.init.count,
                "page": $scope.init.page,
                "pages": 0,
                "size": 0
            },
            "ssortBy": $scope.init.sortBy,
            "sortOrder": $scope.init.sortOrder
        };

        /**
        * Elimina los elementos del objeto que son nulos
        * @function
        */
        $scope.deleteUndefinedValues = function (object) {
            for (var key in object){
                if (!object[key]) {
                    delete object[key];
                }
            }
        };

        /**
         * Se encarga de eliminar el recurso
         * @function
         */
        $scope.eliminar = function (recurso) {
            var confirm = confirmService.showConfirm('¿Está seguro de eliminar el recurso?')
                .then(function(answer) {
                    $scope.service.eliminar(recurso)
                        .then(function (data) {
                            $scope.limpiar();
                            $location.url($scope.config.recurso);
                            Message.ok("El registro se ha eliminado exitosamente.");
                        }).catch(function (data, code) {
                        Message.error("No se pudo realizar la operación");
                    });
                }, function() {
                    return;
                });
        };

        /**
         * Se encarga de recuperar la lista paginada de los datos.
         * @function
         */
        $scope.getResource = function (params, paramsObj) {
            paramsObj.sortOrder = paramsObj.sortOrder == 'dsc' ? "DESC" : "ASC";
            $scope.loading = true;
            if (paramsObj.filters){
                $scope.deleteUndefinedValues(paramsObj.filters);
            }
            paramsObj.filtros = paramsObj.filters;
            paramsObj.cantidad = paramsObj.count;
            //delete paramsObj.count;
            $scope.config.pagination.count = paramsObj.cantidad;
            delete paramsObj.filters;
            if(paramsObj.page == 1){
                paramsObj.inicio = 0;
            }else
            if(paramsObj.page > 0){
                paramsObj.inicio = (paramsObj.page - 1)*paramsObj.cantidad;
            }
            paramsObj.orderBy = paramsObj.sortBy;
            paramsObj.orderDir = paramsObj.sortOrder;
            return this.service.listar(paramsObj)
                .then(function (response) {
                    $scope.loading = false;
                    $scope.config.rows = response.data.lista;
                    $scope.config.pagination.size = response.data.total;
                    $scope.config.pagination.pages = Math.ceil(response.data.total / $scope.config.pagination.count);
                    return $scope.config;
                }).catch(function(response){
                    console.error(response);
                    $scope.loading = null;
                    $scope.config.rows = [];
                    $scope.config.pagination.size =0;
                    $scope.config.pagination.pages=0;
                    return $scope.config;
                });
        };


        /**
         * Constructor / Entrypoint
         * @constructor
         */

    }
]);
